from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import CreateUserForm
from django.contrib import messages
# Create your views here.

def webpage1(request):
    return render(request, 'home/welcomepage.html')

def logoutUser(request):
    logout(request)
    return redirect('webpage1')

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('webpage1')

    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('webpage1')
            
            else:
                messages.info(request, 'Nama anda atau password anda salah...')
            
    context = {}
    return render(request, 'home/login.html', context)

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('webpage1')

    else:    
        form = CreateUserForm()
        if request.method =="POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "Akun " + user + " sudah dibuat")
                return redirect('login')

    context = {'form':form}
    return render(request, 'home/register.html', context)

